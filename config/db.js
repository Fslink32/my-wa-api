const config = require('./config.json');
const mysql = require('mysql2/promise');
initialize()


async function initialize() {
    // create db if it doesn't already exist
    const {
        username,
        password,
        database,
        host,
        dialect,
        port
    } = config.development;
    const connection = await mysql.createConnection({
        host,
        user: username,
        password,
        port
    });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);
    await connection.end();
    console.log('success');
}