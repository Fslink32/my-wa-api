const {
    Sequelize
} = require('sequelize');
const config = require('./config.json');
const mysql = require('mysql2/promise');

const {
    username,
    password,
    database,
    host,
    dialect,
    port
} = config.development;
const sequelize = new Sequelize(database, username, password, {
    host: host,
    dialect: dialect
});
sequelize.sync({
    logging: false
});

module.exports = sequelize;