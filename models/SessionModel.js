const {
    Model,
    DataTypes
} = require('sequelize');
const sequelize = require('../config/database');

class Session extends Model {}

Session.init({
    token_id: DataTypes.INTEGER,
    session_name: DataTypes.STRING,
    status: DataTypes.STRING,
    phone_number: DataTypes.STRING
}, {
    sequelize,
    modelName: 'Session'
});
sequelize.sync({
    logging: false
});

module.exports = Session;