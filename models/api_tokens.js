const {
    Model,
    DataTypes
} = require('sequelize');
const sequelize = require('../config/database');

class ApiToken extends Model {}

ApiToken.init({
    token: DataTypes.STRING,
}, {
    sequelize,
    modelName: 'apitoken'
});
sequelize.sync({
    logging: false
});

module.exports = ApiToken;