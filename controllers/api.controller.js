class ControllerApi {
    constructor() {
        const crypto = require('crypto');
        const {
            SessionManager
        } = require('wa-multi-session')
        const {
            toDataURL
        } = require("qrcode");
        const {
            processNumber
        } = require("../utils/process-number");
        this.axios = require("axios");
        this.express = require("express");
        this.https = require('https');
        this.fs = require('fs');
        this.processNumber = processNumber;
        this.createDelay = require("../utils/create-delay");
        this.whatsapp = require('wa-multi-session');
        this.SessionManager = SessionManager;
        this.toDataURL = toDataURL;
        this.generateKeys = crypto;
        this.ApiToken = require('../models/api_tokens');
        this.Session = require('../models/SessionModel');
    }

    findSessionName(sessionId) {
        try {
            const data = this.Session.findOne({
                where: {
                    session_name: sessionId
                }
            })
            return !data ? false : data;
        } catch (error) {

        }
    }

    getNameFromSessionData(data) {
        const SessionName = [];
        for (const dt of data) {
            const sessiondata = {
                session_name: dt.session_name,
                status: dt.status,
                phone_number: dt.phone_number
            }
            SessionName.push(sessiondata);
        }
        return SessionName;
    }

    checkSessionState(session) {
        const sessionName = this.whatsapp.getAllSession();
        console.log(sessionName);
        if (sessionName.indexOf(session) == -1) {
            return false;
        } else {
            return true
        }
    }

    findSessionNameByToken(token_id, sessionName = null) {
        try {
            var data;
            if (!sessionName) {
                data = this.Session.findAll({
                    where: {
                        token_id
                    },
                    logging: false
                })
            } else {
                data = this.Session.findOne({
                    where: {
                        token_id,
                        session_name: sessionName
                    },
                    logging:false
                })
            }
            const condition = !data ? false : data;
            return condition;
        } catch (error) {}
    }

    tbSessionUpdateStatus(sessionName, column) {
        try {
            const data = this.Session.update({
                status: column.status,
                phone_number: column.phone_number,
            }, {
                where: {
                    session_name: sessionName
                },
                logging:false
            });

            if (!data) {
                return false;
            } else {
                return true;
            }
        } catch (err) {}
    }

    async getAllSession(req, res) {
        try {
            const data = await this.findSessionNameByToken(req.data.id);
            const sessionName = await this.getNameFromSessionData(data)
            if (!req.data) {
                res.status(400).json({
                    status: false,
                    data: {
                        error: "Token is invalid",
                    },
                });
            } else {
                res.status(200).json({
                    status: true,
                    data: {
                        sessionId: sessionName
                    },
                });
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }

    async getSession(req, res) {
        try {
            const token = req.query.token;
            const session = req.query.session;
            const sessions = this.findSessionName(session, token);
            if (!sessions) {
                res.status(400).json({
                    status: false,
                    data: {
                        error: "session name is invalid",
                    },
                });
            } else {
                res.status(200).json({
                    status: true,
                    data: {
                        sessions,
                        sessionId: session
                    },
                });
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }

    async createSession(req, res) {
        try {
            const {
                EventEmitter
            } = require('events');
            const sessionEmitters = new Map(); // Map to store event emitters for each session
            var sessionId = req.query.sessionId;
            const scan = req.query.scan;
            var sessionName;
            var next = false;
            if (!req.query.sessionId) {
                sessionId = await this.generateKeys.randomBytes(Math.ceil(10 / 2))
                    .toString('hex')
                    .slice(0, 10);
                const session = await this.Session.create({
                    token_id: parseInt(req.data.id),
                    session_name: sessionId,
                    status: "Tidak Aktif"
                });
                next = true;
            }
            const data = await this.findSessionNameByToken(req.data.id, sessionId);
            if (!data) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Session Not Found",
                    },
                });
            } else {
                sessionName = sessionId;
            }
            if (!sessionName) {
                throw new Error("Bad Request");
            }

            if (res && !res.headersSent) {
                res.writeHead(200, {
                    'Content-Type': 'text/event-stream',
                    'Cache-Control': 'no-cache',
                    'Connection': 'keep-alive'
                });
            }

            // Get or create an event emitter for the session
            let eventEmitter = sessionEmitters.get(sessionName);
            if (!eventEmitter) {
                eventEmitter = new EventEmitter();
                sessionEmitters.set(sessionName, eventEmitter);
            }

            // Emit data to the event emitter when QR code is updated or session is connected
            this.whatsapp.onQRUpdated(async (data) => {
                var qr = await this.toDataURL(data.qr);
                if (scan && data.sessionId == sessionName) {
                    res.render("scan", {
                        qr
                    });
                } else {
                    var eventData = {
                        sessionName,
                        qr
                    };
                    eventEmitter.emit('update', eventData);
                }
            });
            await this.whatsapp.startSession(sessionName, {
                printQR: true
            });
            
            await this.whatsapp.onConnected(async (sessionId) => {
                clearTimeout(stopqr);
                const number = await this.whatsapp.getSession(sessionId);
                await this.tbSessionUpdateStatus(sessionId, {
                    status: "Aktif",
                    phone_number: "+" + number.user.id.split(":")[0]
                });
                var eventData = {
                    sessionName,
                    status:true
                };
                eventEmitter.emit('update', eventData);
                res.end();
                return;
            });
            const stopqr = setTimeout(() => {
                res.end();
                eventEmitter.removeAllListeners();
            }, 30000);
            
            // Send data to the client when the event emitter emits 'update' event
            eventEmitter.on('update', (eventData) => {
                res.write(`data: ${JSON.stringify(eventData)}\n\n`);
            });
            
        } catch (error) {
            // console.log(error);
            res.status(400).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }

    async getSessionData(req, res) {
        try {
            const sessionId = req.query.sessionId;
            const session = await this.whatsapp.getSession(sessionId);
            await res.status(200).json({
                status: true,
                data: {
                    data: "+" + session.user.id.split(":")[0]
                },
            });
        } catch (error) {
            res.status(500).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }

    async generateToken(req, res) {
        try {
            const token = this.generateKeys.randomBytes(32).toString('hex');
            const session_name = this.generateKeys.randomBytes(Math.ceil(10 / 2))
                .toString('hex')
                .slice(0, 10);
            const apiToken = await this.ApiToken.create({
                token
            });
            res.status(200).json({
                status: true,
                data: {
                    token: token,
                },
            });
        } catch (err) {
            console.error(err);
            res.status(500).json({
                message: 'Server error'
            });
        }
    }

    async deleteSession(req, res) {
        try {
            const sessionName = req.body.sessionId || req.query.sessionId || req.headers.sessionId;
            const data = await this.findSessionNameByToken(req.data.id, sessionName);
            console.log(this.checkSessionState(sessionName));
            if (!await this.checkSessionState(sessionName) || !data) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Session Not Found",
                    },
                });
            }
            this.whatsapp.deleteSession(sessionName);
            this.tbSessionUpdateStatus(sessionName, {
                status: "Tidak Aktif",
                phone_number: null
            })
            res.status(200).json({
                status: true,
                data: {
                    message: "Success Deleted " + sessionName,
                },
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }

    async getMessage(req, res) {
        try {
            const sessionName = req.body.sessionId || req.query.sessionId || req.headers.sessionId;
            const data = await this.findSessionNameByToken(req.data.id, sessionName);
            if (!await this.checkSessionState(sessionName) || !data) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Session Not Found",
                    },
                });
            }
            this.whatsapp.onMessageReceived(async (msg) => {
                if (msg.sessionId === sessionName) {
                    res.status(200).json({
                        status: true,
                        data: {
                            message: msg.message.conversation,
                        },
                    });
                }
            });
        } catch (error) {
            // console.log(error);
            res.status(400).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }

    async sendMessage(req, res) {
        try {
            let to = req.body.to || req.query.to,
                text = req.body.text || req.query.text;
            let isGroup = req.body.isGroup || req.query.isGroup;
            console.log(
                "message send from >",
                req.headers["x-forwarded-for"] || req.socket.remoteAddress
            );
            const sessionId =
                req.body.sessionId || req.query.sessionId || req.headers.sessionId;
            const data = await this.findSessionNameByToken(req.data.id, sessionId);
            if (!to)
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Bad Reques",
                    },
                });
            if (!to || !text)
                throw new Error("Tujuan dan Pesan Kosong atau Tidak Sesuai");

            const receiver = this.processNumber(to);
            if (!await this.checkSessionState(sessionId) || !data) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Session Not Found",
                    },
                });
            }

            const send = await this.whatsapp.sendTextMessage({
                sessionId,
                to: receiver,
                text,
            });

            res.status(200).json({
                status: true,
                data: {
                    id: send.key.id,
                    status: send.status,
                    message: send.message.extendedTextMessage.text || "Not Text",
                    remoteJid: send.key.remoteJid,
                },
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }

    async sendBulkMessage(req, res) {
        try {
            const sessionId =
                req.body.session || req.query.session || req.headers.session;
            const delay =
                req.body.delay || req.query.delay || req.headers.delay;
            const text =
                req.body.text || req.query.text || req.headers.text;
            if (!await this.checkSessionState(sessionId)) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Session Not Found",
                    },
                });
            }
            for (const dt of req.body.data) {
                await this.createDelay(delay);
                await this.whatsapp.sendTextMessage({
                    sessionId,
                    to: this.processNumber(dt.to),
                    text: text,
                });
            }
            console.log("SEND BULK MESSAGE WITH DELAY SUCCESS");

            res.status(200).json({
                status: true,
                data: {
                    message: "Bulk Message is Processing",
                },
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }

    async sendImage(req, res) {
        try {
            const formData = await new Response(req, {
                headers: req.headers
            }).formData()
            let to = formData.get('to') || req.query.to || req.body.to,
                text = formData.get('text') || req.query.text || req.body.text,
                url = formData.get('url') || req.query.url || req.body.url;
            let isGroup = req.body.isGroup || req.query.isGroup;
            let lokasi;
            let filename;
            const sessionId =
                formData.get('session') || req.query.session || req.headers.session || req.body.session;
            const directory = './public/temp';
            if (!url) {
                const file = formData.get('file');
                filename = file.name;
                lokasi = directory + '/' + filename;
                const buffer = await file.arrayBuffer();
                this.fs.writeFileSync(lokasi, Buffer.from(buffer), (err) => {
                    if (err) {
                        console.error(err);
                        res.statusCode = 500;
                        res.end('Error saving file');
                    }
                });
            } else {
                const array = url.split('/');

                // get the last element // replace with the destination file path
                filename = array[array.length - 1];
                lokasi = directory + '/' + filename;
                // console.log(filepath);
                // return;
                const buffer = await new Promise((resolve, reject) => {
                    this.axios
                        .get(url, {
                            responseType: "arraybuffer",
                        })
                        .then((response) => {
                            resolve(response);
                        })
                        .catch((err) => {
                            console.error(err);
                            reject(err);
                        });
                });;

                this.fs.writeFileSync(lokasi, buffer.data);
            }
            const filepath = lokasi;

            const filetype = ["jpg", "jpeg", "png", "gif", "bmp", "svg", "tif", "tiff", "webp"];
            let extarray = filename.split('.');
            const ext = extarray[extarray.length - 1];
            if (filetype.indexOf(ext) == -1) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "is not an image",
                    },
                });
            }

            const receiver = this.processNumber(to);
            if (!await this.checkSessionState(sessionId)) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Session Not Found",
                    },
                });
            }
            // console.log(filepath);
            // return;

            const send = await this.whatsapp.sendImage({
                sessionId,
                to: receiver,
                text,
                isGroup: isGroup,
                media: filepath
            });


            this.fs.readdir(directory, (err, files) => {
                if (err) throw err;

                for (const file of files) {
                    this.fs.unlink(`${directory}/${file}`, err => {
                        if (err) throw err;
                    });
                }
            });

            res.status(200).json({
                status: true,
                data: {
                    id: send.key.id,
                    status: send.status,
                    message: text || "Not Text",
                    remoteJid: send.key.remoteJid,
                },
            });

        } catch (error) {
            console.log(error);
            return res.send({
                status: 500,
                message: "Internal Server Error"
            });
        }
    }

    async sendVideo(req, res) {
        try {
            const formData = await new Response(req, {
                headers: req.headers
            }).formData()
            let to = formData.get('to') || req.query.to || req.body.to,
                text = formData.get('text') || req.query.text || req.body.text,
                url = formData.get('url') || req.query.url || req.body.url;
            let isGroup = req.body.isGroup || req.query.isGroup;
            let lokasi;
            let filename;
            const sessionId =
                formData.get('session') || req.query.session || req.headers.session || req.body.session;
            const directory = './public/temp';
            if (!url) {
                const file = formData.get('file');
                filename = file.name;
                lokasi = directory + '/' + filename;
                const buffer = await file.arrayBuffer();
                this.fs.writeFileSync(lokasi, Buffer.from(buffer), (err) => {
                    if (err) {
                        console.error(err);
                        res.statusCode = 500;
                        res.end('Error saving file');
                    }
                });
            } else {
                const array = url.split('/');

                // get the last element // replace with the destination file path
                filename = array[array.length - 1];
                lokasi = directory + '/' + filename;
                // console.log(filepath);
                // return;
                const buffer = await new Promise((resolve, reject) => {
                    this.axios
                        .get(url, {
                            responseType: "arraybuffer",
                        })
                        .then((response) => {
                            resolve(response);
                        })
                        .catch((err) => {
                            console.error(err);
                            reject(err);
                        });
                });;

                this.fs.writeFileSync(lokasi, buffer.data);
            }
            const filepath = lokasi;

            const filetype = ['mp4', 'mkv', 'avi', 'mov', 'wmv', 'flv', 'webm', 'm4v'];
            let extarray = filename.split('.');
            const ext = extarray[extarray.length - 1];
            if (filetype.indexOf(ext) == -1) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "is not an image",
                    },
                });
            }

            const receiver = this.processNumber(to);
            if (!await this.checkSessionState(sessionId)) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Session Not Found",
                    },
                });
            }
            // console.log(filepath);
            // return;

            const send = await this.whatsapp.sendVideo({
                sessionId,
                to: receiver,
                text,
                isGroup: isGroup,
                media: filepath
            });


            this.fs.readdir(directory, (err, files) => {
                if (err) throw err;

                for (const file of files) {
                    this.fs.unlink(`${directory}/${file}`, err => {
                        if (err) throw err;
                    });
                }
            });

            res.status(200).json({
                status: true,
                data: {
                    id: send.key.id,
                    status: send.status,
                    message: text || "Not Text",
                    remoteJid: send.key.remoteJid,
                },
            });

        } catch (error) {
            console.log(error);
            return res.send({
                status: 500,
                message: "Internal Server Error"
            });
        }
    }

    async sendTyping(req, res) {
        try {
            const formData = await new Response(req, {
                headers: req.headers
            }).formData()
            let to = formData.get('to') || req.query.to || req.body.to,
                duration = formData.get('duration') || req.query.duration || req.body.duration;
            let isGroup = req.body.isGroup || req.query.isGroup;
            const sessionId =
                formData.get('session') || req.query.session || req.headers.session || req.body.session;

            if (!await this.checkSessionState(sessionId)) {
                return res.status(400).json({
                    status: false,
                    data: {
                        error: "Session Not Found",
                    },
                });
            }
            let durasi = parseInt(duration);
            const receiver = this.processNumber(to);
            const send = await this.whatsapp.sendTyping({
                sessionId,
                to: receiver,
                durasi
            });
            res.status(200).json({
                status: true,
                data: {
                    duration: duration || "00",
                },
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({
                status: false,
                data: {
                    error: error.message,
                },
            });
        }
    }


}

module.exports = ControllerApi;