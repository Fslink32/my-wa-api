const express = require('express');
const router = express.Router();
const Api = require('../controllers/api.controller');

const controller = new Api();
const apiv = '/api/v1'

async function validateToken(req, res, next) {
    try {
        const ApiToken = require("../models/api_tokens")
        const token = req.query.token;
        const data = await ApiToken.findOne({
            where: {
                token: token
            },
            logging: false
        });
        if (!data) {
            res.status(400).json({
                error: 'Invalid Token'
            })
        } else {
            req.data = data;
            next();
        }
    } catch (error) {}
}

router.post(apiv + '/sendbulkmessage', validateToken, controller.sendBulkMessage.bind(controller));
router.get(apiv + '/sessions', validateToken, controller.getAllSession.bind(controller));
router.get(apiv + '/create-session', validateToken, controller.createSession.bind(controller));
router.get(apiv + '/delete-session', validateToken, controller.deleteSession.bind(controller));
router.post(apiv + '/send-message', validateToken, controller.sendMessage.bind(controller));
router.post(apiv + '/send-image', validateToken, controller.sendImage.bind(controller));
router.post(apiv + '/send-video', validateToken, controller.sendVideo.bind(controller));
router.post(apiv + '/send-typing', validateToken, controller.sendTyping.bind(controller));
router.get(apiv + '/generatetoken', controller.generateToken.bind(controller));
router.get(apiv + '/getmessage', validateToken, controller.getMessage.bind(controller));
router.get(apiv + '/getsessiondata', validateToken, controller.getSessionData.bind(controller));

module.exports = router;